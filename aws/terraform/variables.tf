variable "profile" {
  default = "terraform_user"
}

variable "region" {
  default = "ca-central-1"
}

variable "instance" {
  default = "t2.micro"
}

variable "instance_count" {
  default = "1"
}

variable "public_key" {
  default = "~/.ssh/pbsa.pub"
}

variable "private_key" {
  default = "~/.ssh/pbsa.pem"
}

variable "ansible_user" {
  default = "ubuntu"
}

variable "ami" {
  # ami-0c27a26eca5dc74fc - Ubuntu 18.04 AMI in ca-central
  default = "ami-0c27a26eca5dc74fc"
}

variable "witness_service" {
  default = "../utilities/ubuntu/witness.service"
}